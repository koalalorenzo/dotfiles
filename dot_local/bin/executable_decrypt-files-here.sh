#!/bin/bash


for i in ./* ;
do
  if [[ $i != *keybase.txt* ]]; then continue; fi
  if [ -d "$i" ]; then continue; fi

  echo -e "\n\033[0;32m#Working on $i \033[0m"
  # NEW_NAME=$(python3 -c "import urllib.parse; print(urllib.parse.quote(input()))" <<< $i)
  decrypt-file $i
done
