#!/usr/bin/env bash

# Initialize an empty array to store file paths
paths=$(find "$3" -mindepth 1 -printf '%P\n')
files=()
# Loop through all arguments starting from the second one (index 1)
for arg in $paths; do
    files+=("$2/$arg")
done

# Use jq to create the JSON output
json_output=$(jq -n --arg files "$(printf '%s\n' "${files[@]}")" \
    '{"files": ($files | split("\n"))}')

curl "https://api.cloudflare.com/client/v4/zones/$1/purge_cache" \
    -H 'Content-Type: application/json' \
    -H "X-Auth-Email: $CACHE_CLOUDFLARE_EMAIL" \
    -H "Authorization: Bearer $CACHE_CLOUDFLARE_API_KEY" \
    -d "$json_output" | jq

