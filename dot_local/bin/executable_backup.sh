#!/usr/bin/env bash
set -u
send-log.sh job=restic-backup starting all backups

set -a
source ~/.config/restic/local.env
set +a

restic-backup.sh \
  $HOME \
  --iexclude-file="$HOME/.config/restic/excludes.txt" \
  --exclude-larger-than=64g \
  $@

set -a
source ~/.config/restic/remote.env
set +a

restic-backup.sh \
  "$HOME/Documents" \
  "$HOME/Library/Mobile Documents" \
  "$HOME/Desktop" \
  --iexclude-file=$HOME/.config/restic/excludes-remote.txt \
  --exclude-larger-than 4g 
  $@

