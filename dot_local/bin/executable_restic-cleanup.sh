#!/usr/bin/env bash
set -eu
source ~/.config/restic/global.sh

log restic-cleanup Starting a new cleanup

# Clean cache
restic cache --cleanup

# Check random data to ensure integrity
caffeinate -i restic check --read-data-subset=0.1%

log restic-cleanup Check completed and starting forget

# Clean old snapshots but always preserve the last one.
caffeinate -i restic forget --compact \
  --keep-last 1 \
  --keep-within-hourly 24h \
  --keep-within-daily 7d \
  --keep-within-weekly 1m \
  --keep-within-monthly 12m \
  --keep-within-yearly 5y \
  --keep-tag keep $@

log restic-cleanup Forget completed and starting prune

caffeinate -i restic prune $@

log restic-cleanup Prune completed
