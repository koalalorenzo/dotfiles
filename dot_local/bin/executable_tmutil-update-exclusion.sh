#!/usr/bin/env bash
EXCLUDE_FILE=$HOME/.config/time-machine/exclude.txt

# Ensure the exclude file exists
if [[ ! -f "$EXCLUDE_FILE" ]]; then
    echo "Error: Exclusion file $EXCLUDE_FILE not found!"
    exit 1
fi

CLEAN_MODE=false
if [[ "$1" == "--clean" ]]; then
    CLEAN_MODE=true
fi

while IFS= read -r expath; do
    # Check that the path is not null and not commented
    if [[ -n "$expath" && ! "$expath" =~ ^# ]]; then
        if [ "$CLEAN_MODE" = true ]; then
            # If the file/dir exists removes it from the exclusion to re-add it
            # This is due to cleanup in tags
            echo "Cleanign exclusion for $HOME/$expath"
            if [ -e "$HOME/$expath" ]; then
                tmutil removeexclusion "$HOME/$expath"
            fi
            sudo tmutil removeexclusion -p "$HOME/$expath"
        fi

        if [ "$CLEAN_MODE" = false ]; then
            if [ -e "$HOME/$expath" ]; then
                echo "Excluding (file): $HOME/$expath"
                tmutil addexclusion "$HOME/$expath"
            else
                echo "Excluding (path): $HOME/$expath"
                sudo tmutil addexclusion -p "$HOME/$expath"
            fi
        fi
        set +xe
    fi
done < "$EXCLUDE_FILE"
