#!/usr/bin/env bash
set -u
source ~/.config/restic/global.sh


TMP_FILE_PREFIX=$(echo "$RESTIC_REPOSITORY" | sed 's/[^a-zA-Z0-9-]//g' | cut -c1-32)
PID_FILE=/tmp/restic-$TMP_FILE_PREFIX-backup.pid
TIMESTAMP_FILE=/tmp/restic-$TMP_FILE_PREFIX-backup.timestamp

check_pid_file "$PID_FILE"
check_timestamp "$TIMESTAMP_FILE"

echo $$ > "$PID_FILE"
log "Starting local backup"

set -o pipefail

caffeinate -i restic backup "$@"

OUTPUT_CODE=$?

if [ $OUTPUT_CODE -eq 0 ]; then
  date -d "+15 minutes" +"%s" > "$TIMESTAMP_FILE"
  log restic-backup backup completed
else
  log restic-backup backup ran with errors
fi

rm "$PID_FILE"
exit $OUTPUT_CODE
