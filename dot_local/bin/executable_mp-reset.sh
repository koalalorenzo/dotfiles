#!/usr/bin/env bash
set -ex
multipass stop --all
multipass delete --all
multipass purge
sleep 1

multipass launch --cpus 4 --disk 16G -m 8G --name primary \
  --cloud-init ~/.config/multipass/cloud-init.yaml

sleep 10
multipass restart

