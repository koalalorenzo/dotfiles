#!/usr/bin/env  bash

if [[ $1 == *keybase.txt* ]]; then
  keybase decrypt -o "${i%.*}" -i "$1"
fi

if [[ $1 == *.gpg* ]]; then
  gpg --decrypt -o "${i%.*}" $@
fi

if [[ $1 == *.asc* ]]; then
  gpg --decrypt -o "${i%.*}" $@
fi

if [[ $1 == *.pgp* ]]; then
  gpg --decrypt -o "${i%.*}" $@
fi
