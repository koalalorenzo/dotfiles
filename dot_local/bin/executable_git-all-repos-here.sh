#!/bin/bash

export ORIGINAL_PWD="$PWD"
for i in ./* ;
do
  cd $ORIGINAL_PWD
  if ! [ -d "$i" ]; then continue; fi
  if ! [ -d "$i/.git" ]; then continue; fi
  cd $i
  echo "Working on $i"
  git $@
done
