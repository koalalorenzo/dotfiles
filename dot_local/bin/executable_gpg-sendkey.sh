#!/bin/bash
set -v
timeout -k 60s 45s gpg --send-key $1
timeout -k 60s 45s gpg --keyserver hkps://keyserver.ubuntu.com --send-key $1
timeout -k 60s 45s gpg --keyserver hkps://hkps.pool.sks-keyservers.net --send-key $1
timeout -k 60s 45s gpg --keyserver hkps://pgp.mit.edu --send-key $1
timeout -k 60s 45s gpg --keyserver hkps://keys.openpgp.org --send-key $1

gpg --export $1 | curl -T - https://keys.openpgp.org

