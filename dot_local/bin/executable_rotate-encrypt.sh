#!/bin/bash
set -e

echo -e "\n\033[0;32m#Rotating Keybase \033[0m"
for i in ./*.keybase.txt ;
do
  [ -f "$i" ] || break
  echo -e "\n\033[0;32m#Working on $i \033[0m"
  # NEW_NAME=$(python3 -c "import urllib.parse; print(urllib.parse.quote(input()))" <<< $i)
  keybase decrypt --force -i "$i" | encrypt.sh -o "${i%.keybase.txt}.gpg"
  rm "$i"
done

echo -e "\n\033[0;32m#Rotating GPG -> New GPG \033[0m"
for i in ./*.gpg ;
do
  echo -e "\n\033[0;32m#Working on $i \033[0m"
  [ -f "$i" ] || break
  gpg -d "$i" | encrypt.sh -o "$i.new.gpg" "$i"
  rm "$i"
  mv "$i.new.gpg" "$i"
done
