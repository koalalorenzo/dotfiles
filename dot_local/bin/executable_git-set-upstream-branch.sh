#!/bin/bash

CURRENT_BRANCH=$(git branch --show-current)

git branch --set-upstream-to=origin/${CURRENT_BRANCH} ${CURRENT_BRANCH}