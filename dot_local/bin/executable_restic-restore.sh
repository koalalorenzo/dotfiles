#!/usr/bin/env bash
source ~/.config/restic/global.sh

if [ $# -eq 0 ]; then
    echo "Error: Please provide a path to restore as an argument."
    exit 1
fi

TMP_DIR=$(mktemp -d)
set -ex
restic restore latest --target "$TMP_DIR" --verify --include $@

echo "- Results available in: $TMP_DIR"
