#!/usr/bin/env bash
set -ex

podman machine rm -f || echo "Machine not present continuing.."

sudo /usr/local/podman/helper/koalalorenzo/podman-mac-helper install

podman machine init --cpus=4 -m=4096 --rootful --now
# podman machine start
sleep 30
podman machine ssh -- sudo sed -i 's/short-name-mode="enforcing"/short-name-mode="permissive"/g' /etc/containers/registries.conf
