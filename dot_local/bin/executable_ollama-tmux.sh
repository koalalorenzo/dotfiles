#! /usr/bin/env nix-shell
#! nix-shell -p bash tmux -i bash

MODEL=${1:-"llama3.1"}

# tmux list-windows
tmux new-window -n ollama -d -- ollama serve
sleep 1
tmux split-window -t ollama -- ollama run "$MODEL"
tmux resize-pane -t :ollama.0 -y 5
tmux selectw -t ollama
