#!/bin/bash

for i in {1..80}; do echo -n "#"; done
echo ""
while IFS= read -r line; do
    echo "# $line"
done <<< $(echo "$@" | fmt -w 78)
for i in {1..80}; do echo -n "#"; done
