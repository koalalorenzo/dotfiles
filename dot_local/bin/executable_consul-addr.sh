#!/usr/bin/env bash

export _TMP_SVC_FILE=$(mktemp)

# Function to display usage
usage() {
    echo "Usage: $0 [-a] [-p] [-d delimiter] service_name"
    echo "  -a            Print only the address"
    echo "  -p            Print only the port"
    echo "  -d delimiter  Use the specified delimiter between address and port"
    echo "  service_name  Name of the service to query"
    exit 1
}

# Default delimiter
DELIMITER=":"

# Parse command-line options
while getopts "apd:" opt; do
    case "$opt" in
        a) ONLY_ADDR=true ;;
        p) ONLY_PORT=true ;;
        d) DELIMITER="$OPTARG" ;;
        *) usage ;;
    esac
done

# Shift to get the service name argument
shift $((OPTIND - 1))

# Check if service name is provided
if [ -z "$1" ]; then
    usage
fi

curl -s -L -o "$_TMP_SVC_FILE" ${CONSUL_HTTP_ADDR}/v1/health/service/$1

ADDR=$(jq -r '.[0].Service.Address' $_TMP_SVC_FILE)
PORT=$(jq -r '.[0].Service.Port' $_TMP_SVC_FILE)

if [ "$ONLY_ADDR" == "true" ]; then
    echo "$ADDR"
elif [ "$ONLY_PORT" == "true" ]; then
    echo "$PORT"
else
    echo "$ADDR$DELIMITER$PORT"
fi
