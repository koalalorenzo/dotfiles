#!/bin/bash


for i in ./* ;
do
  if [[ $i == *gpg* ]]; then continue; fi
  if [[ $i == *pgp* ]]; then continue; fi
  if [[ $i == *asc* ]]; then continue; fi
  if [[ $i == *keybase.txt* ]]; then continue; fi
  if [ -d "$i" ]; then continue; fi

  echo -e "\n\033[0;32m#Working on $i \033[0m"
  # NEW_NAME=$(python3 -c "import urllib.parse; print(urllib.parse.quote(input()))" <<< $i)
  encrypt-file.sh "$i"
done
