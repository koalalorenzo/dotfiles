#!/usr/bin/env bash

sudo -i sh -c 'launchctl load /Library/LaunchDaemons/org.nixos.nix-daemon.plist'
sudo -i sh -c 'launchctl load /Library/LaunchDaemons/org.nixos.darwin-store.plist'
sudo -i sh -c 'launchctl load /Library/LaunchDaemons/systems.determinate.nix-installer.nix-hook.plist'

sleep 1
sudo launchctl enable system/org.nixos.nix-daemon
sudo launchctl enable system/org.nixos.darwin-store
sudo launchctl enable system/systems.determinate.nix-installer.nix-hook

