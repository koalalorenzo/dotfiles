#!/usr/bin/env bash

if [[ -d ~/.nix-profile/Applications ]]; then
	cd ~/.nix-profile/Applications;
	for f in *.app ; do
		mkdir -p ~/Applications/
		rm -f "$HOME/Applications/$f"
		# Mac aliases don’t work on symlinks
		fl="$(readlink -f "$f")"
		# Use Mac aliases because Spotlight doesn’t like symlinks
		osascript -e "tell app \"Finder\" to make new alias file at POSIX file \"$HOME/Applications\" to POSIX file \"$fl\""
		rm -f "$HOME/Applications/$f alias"
	done
fi