#!/usr/bin/env bash
# Send log line to Loki, used with Grafana Cloud in mind

source $HOME/.config/rcs/secretenv

# Parse labels from arguments
LABELS="\"instance\": \"$HOSTNAME\", \"level\":\"info\", \"source\":\"send-log.sh\""
while [[ $# -gt 0 ]]; do
    case "$1" in
        *=*)
            KEY=$(echo "$1" | cut -d '=' -f 1)
            VALUE=$(echo "$1" | cut -d '=' -f 2-)
            # Preserve quotes around value
            LABELS="${LABELS}, \"${KEY}\": \"${VALUE}\""
            shift 1
            ;;
        *)
            break
            ;;
    esac
done

# Remove leading comma
LABELS="${LABELS#,}"

# Define log text
LOG_TEXT="$@"

# Send log to Loki#
curl --silent --output /dev/null --show-error \
     -XPOST -H "Content-Type: application/json" --data @- ${LOKI_URL}/loki/api/v1/push <<EOF
{
  "streams": [
    {
      "stream": {$LABELS},
      "values": [
        [ "$(date +"%s%N")", "${LOG_TEXT}" ]
      ]
    }
  ]
}
EOF
