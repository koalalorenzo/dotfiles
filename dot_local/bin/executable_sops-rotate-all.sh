#!/usr/bin/env bash
set -ex

find . -type f -name "*.sops.*" -exec sops -i -r {} \;