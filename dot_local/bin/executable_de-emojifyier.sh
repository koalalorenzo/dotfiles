#!/bin/bash

emojifile=$(mktemp)

curl https://raw.githubusercontent.com/CodeFreezr/emojo/master/db/v5/emoji-v5.json --output $emojifile

total=($(cat $emojifile | jq -r '.[] | "\(.Emoji)=\(.Shortcode)" | @sh' | tr \' " "))

echo $total
# For each emoji run
for i in ${total[@]}; do
  emoji=$(echo $total[$i] | sed 's/=.*//')
  shortcode=$(echo $total[$i] | sed 's/.*=//')
  echo "Replacing $emoji with $shortcode"
  # For each file found
  for file in $(rg "$emoji" -l); do
    echo $file
    rg --passthru "$emoji" -r "$shortcode" "$file" | sponge $file
  done
done