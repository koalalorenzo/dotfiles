#!/bin/bash
set -evx


# Encrypt using GnuPG from Yubico and ProtonMail
gpg -a \
  -r 73880ECAF69EC2ED44CE5889502BFB12D0B5295F \
  -r lorenzo@setale.me \
  --encrypt "$@"
