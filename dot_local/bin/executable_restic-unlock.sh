#!/usr/bin/env bash
set -eu
source ~/.config/restic/global.sh

restic unlock $@
