#! /usr/bin/env nix-shell
#! nix-shell -i bash -p imagemagick libwebp

for file in "$@"
do
  echo "Converting $file"
  cwebp -short -q 85 "$file" -mt -o "$(basename $file).webp"
done
