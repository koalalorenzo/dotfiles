#!/usr/bin/env nix-shell
#! nix-shell -i bash -p fdupes

fdupes \
  --cache \
  --nohidden \
  --noempty \
  --time \
  --size \
  --recurse \
  $@
