#!/bin/bash
# shiori settings
shiori_URL="$1"
Username="$2"
read -s -p "$2's password: " Password

echo ""
echo "-- Generating the token --"

token=$(
  curl -X POST \
    -H "Content-Type: application/json" \
    -d '{"username": "'"$Username"'" , "password": "'"$Password"'", "remember_me": true}' \
    $shiori_URL/api/v1/auth/login | jq -r .message
)

echo "Token stored in Apple Shortcuts (iCloud) shiori-token.txt"
echo $token > "$HOME/Library/Mobile Documents/iCloud~is~workflow~my~workflows/Documents/shiori-token.txt"
