#!/usr/bin/env bash

export date=$(date +%s)

SERVICE_ACCOUNT="default"
DNS=""
TTL="3600"
IMAGE="alpine:edge"
TOLERATION_KEY=""
TOLERATION_VALUE=""
HOST_PATH=""
AFFINITY_KEY=""
AFFINITY_VALUE=""
GPU=""

_TMP_FILE=$(mktemp)

while getopts 's:d:h:i:t:a:A:k:v:c:C:g:' OPTION; do
  case "$OPTION" in
    s) SERVICE_ACCOUNT="$OPTARG" ;;
    d) DNS="$OPTARG" ;;
    t) TTL="$OPTARG" ;;
    c) TOLERATION_KEY="$OPTARG" ;;
    C) TOLERATION_VALUE="$OPTARG" ;;
    g) GPU="$OPTARG" ;;
    h) HOST_PATH="$OPTARG" ;;
    i) IMAGE="$OPTARG" ;;
    a) AFFINITY_KEY="$OPTARG" ;;
    A) AFFINITY_VALUE="$OPTARG" ;;
    ?)
      echo -e "Usage: $(basename $0) [-s service_account_name] [-d dns_nameserver] [-t ttl_secs]\n"
      exit 1
      ;;
  esac
done


echo "Creating container...."

cat >> $_TMP_FILE <<EOF
apiVersion: v1
kind: Pod
metadata:
  name: lorenzo-shell-${date}
  labels:
    app: lorenzo-shell
spec:
  serviceAccountName: ${SERVICE_ACCOUNT}
  containers:
  - name: shell
    image: ${IMAGE}
    command: ["sleep", "${TTL}"]
EOF

if [[ -n "${HOST_PATH}" ]]; then
echo "Adding volume mount for ${HOST_PATH}"
cat >> $_TMP_FILE <<EOF
    volumeMounts:
    - mountPath: ${HOST_PATH}
      name: hostmounteed
EOF
fi

if [[ -n "${DNS}" ]]; then
echo "Adding DNS server ${DNS}"
cat >> $_TMP_FILE <<EOF
  dnsConfig:
    nameservers:
      - ${DNS}
    options:
      - name: ndots
        value: '2'
EOF
fi

if [[ -n "${AFFINITY_KEY}" ]]; then
echo "Adding Node Affinity (${AFFINITY_KEY} = ${AFFINITY_VALUE})"
set -e
# checks if the node exists
echo "Nodes selected:"
kubectl get nodes -l "${AFFINITY_KEY}=${AFFINITY_VALUE}"
set +e
cat >> $_TMP_FILE <<EOF
  affinity:
    nodeAffinity:
      requiredDuringSchedulingIgnoredDuringExecution:
        nodeSelectorTerms:
        - matchExpressions:
          - key: ${AFFINITY_KEY}
            operator: In
            values:
            - ${AFFINITY_VALUE}
EOF
fi

if [[ -n "${HOST_PATH}" ]]; then
echo "Adding Volume for hostPath: ${HOST_PATH}"
cat >> $_TMP_FILE <<EOF
  volumes:
  - name: hostmounteed
    hostPath:
      path: ${HOST_PATH}
EOF
fi

if [[ -n "${TOLERATION_KEY}" ]]; then
echo "Adding Tolerations ${TOLERATION_KEY}=${TOLERATION_VALUE}"
cat >> $_TMP_FILE <<EOF
  tolerations:
  - key: "${TOLERATION_KEY}"
    operator: "Equal"
    value: "${TOLERATION_VALUE}"
    effect: "NoSchedule"
EOF
if [[ -n "${GPU}" ]]; then
echo "Adding GPU nvidia toleration"
cat >> $_TMP_FILE <<EOF
  - key: "nvidia.com/gpu"
    operator: "Equal"
    value: "present"
    effect: "NoSchedule"
EOF
fi
fi

echo "Work File: ${_TMP_FILE}"

set -e

kubectl apply -f $_TMP_FILE

echo "Waiting to have lorenzo-shell-${date} to be ready..."
kubectl wait --for=condition=Ready "pod/lorenzo-shell-${date}"

echo "Attaching... "
kubectl exec --stdin --tty "lorenzo-shell-${date}" --container=shell -- /bin/sh
sleep 1
echo "Deleting in background... "
kubectl delete --wait=false --now "pod/lorenzo-shell-${date}"
