.direnv
.diffusionbee
*CACHE*
*Cache*
*Library/**/Caches
*Library/*Cache*
*Library/Caches*
*Steam*
*cache*
*node_modules*
*vendor*
.Trash
.ansible
.cache
.cmake
.config/containers/podman
.diffusionbee/downloads
.docker
.git
.ipfs
.khoj
.local/share/atuin
.local/share/containers
.local/share/containers/podman
.npm
.oh-my-zsh
.ollama
.restic_backup_timestamp
.restic_remote_backup.pid
.rustup/downloads
.rustup/tmp
.rustup/toolchains
.rustup/updated-hashes
.ssb
.ssh
.tor
.vagrant.d/boxes
.vscode
.zcompdump
.zcompdump*
.zsh*
Caches
GoCode
Kiwix
Library/Application Support/CrossOver
Library/Application Support/Steam
Library/Application Support/nomic.ai/GPT4All
Library/Application\ Support
Library/Application\ Support/Chromium
Library/Application\ Support/CrossOver
Library/Application\ Support/Firefox
Library/Application\ Support/Google
Library/Application\ Support/MobileSync/Backup
Library/Application\ Support/Steam
Library/Application\ Support/minecraft
Library/Application\ Support/nomic.ai/GPT4All
Library/Applocatoom\ Support/Docker\ Desktop
Library/Assistant
Library/Biome
Library/Caches
Library/Containers
Library/Cookies
Library/Google
Library/Group Containers
Library/HomeKit
Library/IdentityServices
Library/IntelligencePlatform
Library/Keychains
Library/LanguageModeling
Library/Logs
Library/Metadata
Library/Preferences/Nextcloud/logs
Library/Preferences/com.apple.networkserviceproxy.plist
Library/Safari
Library/Suggestions
Library/iTunes
Movies
Movies/TV
Music/Music
Parallels
Repositories/**/.git
Steam Library
Steam\ Library
Virtual\ Machines*
cache
go
node_modules
~/Library/Caches*
~/Virtual\ Machines*
