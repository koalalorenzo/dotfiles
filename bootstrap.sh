#!/bin/bash

# Bootstrap the dotfile repo.

sh -c "$(curl -fsLS get.chezmoi.io/lb)" -- init --apply gitlab.com/koalalorenzo/dotfiles
