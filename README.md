# Lorenzo's dotfiles

This repository contains my dotfiles. It is managed using 
[chezmoi](http://chezmoi.io/).

## Requirements:

- GPG

You can get them and pre-install Nix

```bash
curl --proto '=https' --tlsv1.2 -sSf -L https://install.determinate.systems/nix | sh -s -- install

nix-shell -p gnupg curl gnumake age
```

## Install

To set it up run:

```bash
sh -c "$(curl -fsLS get.chezmoi.io/lb)" -- init --apply git.elates.com/koalalorenzo/dotfiles
```
